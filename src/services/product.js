import api from './api';

export const ProductService = {
  list: async () => {
    return (
      await api.get('/list')
    ).data;
  },
  byCategoryId: async (id) => {
    return (
      await api.get(`/${id}`)
    ).data;
  },
};