import React from 'react';

import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faBars,
  faTimes,
  faSearch,
  faChevronLeft,
  faChevronRight,
  faTh,
  faThList,
} from '@fortawesome/free-solid-svg-icons';
import GlobalStyle from './styles/GlobalStyle';
import Routes from './Routes';

library.add(
  faBars,
  faTimes,
  faSearch,
  faChevronRight,
  faTh,
  faThList,
  faChevronLeft
);

function App() {
  return (
    <>
      <GlobalStyle />
      <Routes />
    </>
  );
}

export default App;
