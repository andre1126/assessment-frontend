import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import Homepage from './pages/Homepage';

export default function Routes() {
  return (
    <BrowserRouter>
      <Route path="/" component={Homepage} />
    </BrowserRouter>
  );
}
