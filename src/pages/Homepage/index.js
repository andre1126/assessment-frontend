import React, { useState, useEffect } from 'react';
import Header from '../../components/Header';
import NavBar from '../../components/NavBar';
import Filters from '../../components/Filters';
import Breadcrumb from '../../components/Breadcrumb';
import ProductList from '../../components/ProductList';
import Footer from '../../components/Footer';
import Loading from '../../components/Loading';

import { ProductContainer } from './styles';
import { ProductService } from '../../services/product';

export default function Homepage() {
  const [products, setProducts] = useState([]);
  const [pageName, setPageName] = useState(undefined);
  const [selectedFilter, setSelectedFilter] = useState({});
  const [categoryId, setCategoryId] = useState(3);
  const [isLoading, setIsLoading] = useState(false);

  const handleCategoryChange = (categoryId, pageName) => {
    setCategoryId(categoryId);
    setSelectedFilter({});
    setPageName(pageName);
  };

  useEffect(() => {
    try {
      setIsLoading(true);

      const filterProducts = (value) => {
        return value.filter.find(
          (filter) => filter[selectedFilter.type] === selectedFilter.value
        );
      };

      const getProducts = async () => {
        const response = await ProductService.byCategoryId(categoryId);

        let items;

        if (selectedFilter) {
          items = response.items.filter(filterProducts);
        }

        setProducts(items);
      };

      getProducts();
    } catch (error) {
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  }, [categoryId, selectedFilter, isLoading]);

  return (
    <div>
      <Header handleCategoryChange={handleCategoryChange} />
      <NavBar handleCategoryChange={handleCategoryChange} />
      <Breadcrumb page={pageName} />
      <ProductContainer>
        <Filters
          setSelectedFilter={setSelectedFilter}
          categoryId={categoryId}
        />
        <ProductList page={pageName} products={products} />
      </ProductContainer>
      <Footer />
      {isLoading ? <Loading /> : null}
    </div>
  );
}
