import styled from 'styled-components';

export const ProductContainer = styled.div`
  display: flex;
  max-width: 90rem;
  margin: 0 auto;
  padding: 0 20px;

  @media (max-width: 768px) {
    flex-direction: column;
  }
`;
