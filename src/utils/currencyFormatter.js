export const currencyFormatter = (value) => {
  return value.toFixed(2).replace('.', ',').replace(/^/, 'R$');
};
