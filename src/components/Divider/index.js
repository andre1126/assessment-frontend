import styled from 'styled-components';

const Divider = styled.hr`
  width: 100%;
  margin: 10px 0;
`;

export default Divider;
