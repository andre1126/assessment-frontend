import React, { useEffect, useState } from 'react';

import { Nav, NavText, NavItem } from './styles';
import { ProductService } from '../../services/product';

export default function NavBar({ handleCategoryChange }) {
  const [data, setData] = useState([]);

  useEffect(() => {
    const getData = async () => {
      const response = await ProductService.list();
      setData(response.items);
    };

    getData();
  }, []);

  return (
    <Nav>
      <NavText>
        <ul>
          <li>
            <NavItem onClick={() => handleCategoryChange(1, 'Página Inicial')}>
              Página Inicial
            </NavItem>
          </li>
          {data.map((item, index) => (
            <li key={index}>
              <NavItem onClick={() => handleCategoryChange(item.id, item.name)}>
                {item.name}
              </NavItem>
            </li>
          ))}
          <li>
            <NavItem>Contatos</NavItem>
          </li>
        </ul>
      </NavText>
    </Nav>
  );
}
