import styled from 'styled-components';

import { Colors } from '../../styles/variable';

export const Nav = styled.nav`
  background: ${Colors.PRIMARY};
  font-size: 28px;

  ul {
    display: flex;
    list-style: none;
    padding: 0px;
    margin: 0px;
  }

  button {
    display: inline-block;
    padding: 10px 10px 15px 10px;
    color: ${Colors.WHITE};
    font-weight: 800;
    font-size: 14px;
    transition: background 0.2s, color 0.2s;

    &:hover {
      background-color: ${Colors.PRIMARY_DARK};
    }

    @media (max-width: 768px) {
      display: none;
    }
  }
`;

export const NavText = styled.div`
  max-width: 90rem;
  margin: 0 auto;
  padding: 0 20px;
`;

export const NavItem = styled.button`
  text-transform: uppercase;
  background: none;
  border: none;
`;
