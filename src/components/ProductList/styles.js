import styled from 'styled-components';

import { Colors } from '../../styles/variable';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

export const ProductListContent = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-row-gap: 50px;
  margin: 20px;

  &:after {
    content: '';
    flex: 52%;
  }

  @media (max-width: 1024px) {
    justify-content: center;
    grid-template-columns: repeat(3, 1fr);
    grid-row-gap: 50px;
  }

  @media (max-width: 760px) {
    justify-content: center;
    grid-template-columns: repeat(1, 1fr);
    grid-row-gap: 50px;
  }
`;

export const ProductListHeader = styled.div`
  margin-left: 25px;

  @media (max-width: 768px) {
    margin: 0;
  }
`;

export const Title = styled.h3`
  @media (max-width: 768px) {
    margin-top: 20px;
  }
  color: ${Colors.PRIMARY_DARK};
`;

export const ProductOrder = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const ProductOrderIcons = styled.div`
  & > :first-child {
    margin-right: 6px;
  }

  @media (max-width: 768px) {
    display: none;
  }
`;

export const ProductOrderSelect = styled.div`
  & > :first-child {
    margin-right: 6px;
  }
`;

export const ProductCardContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: auto;
  margin: 0 10px 20px;
`;

export const ProductImage = styled.img`
  width: 230px;
  border: 1px solid ${Colors.GRAY};
  height: 220px;
  object-fit: contain;
`;

export const ProductName = styled.p`
  color: ${Colors.GRAY};
  text-align: center;
`;

export const HigherCardContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const BottomCardContainer = styled.div`
  width: 100%;
`;

export const PriceContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  font-size: 24px;
  margin: 15px 0 5px;
  width: 100%;
`;

export const ProductDiscount = styled.p`
  color: ${Colors.GRAY};
  text-decoration: line-through;
  font-size: 16px;
`;

export const ProductPrice = styled.p`
  color: ${Colors.PRIMARY_DARK};
  text-align: center;
  font-weight: bold;
`;

export const ShopButton = styled.button`
  background-color: ${Colors.SECONDARY};
  border: none;
  border-radius: 6px;
  width: 100%;
  font-size: 18px;
  padding: 8px;
  color: ${Colors.WHITE};
`;
