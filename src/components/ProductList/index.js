import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import {
  Container,
  ProductListHeader,
  ProductListContent,
  Title,
  ProductOrder,
  ProductOrderIcons,
  ProductOrderSelect,
  ProductCardContainer,
  HigherCardContainer,
  ProductImage,
  ProductName,
  BottomCardContainer,
  PriceContainer,
  ProductDiscount,
  ProductPrice,
  ShopButton,
} from './styles';

import { Colors } from '../../styles/variable';
import { currencyFormatter } from '../../utils/currencyFormatter';
import Divider from '../../components/Divider';

export default function ProductList({ products, page = 'Página Inicial' }) {
  return (
    <Container>
      <ProductListHeader>
        <Title>{page}</Title>
        <Divider />
        <ProductOrder>
          <ProductOrderIcons>
            <FontAwesomeIcon color={Colors.SECONDARY} icon="th" size="1x" />
            <FontAwesomeIcon color={Colors.GRAY} icon="th-list" size="1x" />
          </ProductOrderIcons>
          <ProductOrderSelect>
            <span>Ordenar por</span>
            <select name="sort">
              <option value="price">Preço</option>
            </select>
          </ProductOrderSelect>
        </ProductOrder>
        <Divider />
      </ProductListHeader>
      <ProductListContent>
        {products.map((item, index) => (
          <ProductCardContainer key={index}>
            <HigherCardContainer>
              <ProductImage
                src={require(`../../assets/images/${item.image.split('/')[1]}`)}
                alt="product"
              />
              <ProductName>{item.name}</ProductName>
            </HigherCardContainer>
            <BottomCardContainer>
              <PriceContainer>
                {item.specialPrice ? (
                  <>
                    <ProductDiscount>
                      {currencyFormatter(item.price)}
                    </ProductDiscount>
                    <ProductPrice>
                      {currencyFormatter(item.specialPrice)}
                    </ProductPrice>
                  </>
                ) : (
                  <ProductPrice>{currencyFormatter(item.price)}</ProductPrice>
                )}
              </PriceContainer>
              <ShopButton>COMPRAR</ShopButton>
            </BottomCardContainer>
          </ProductCardContainer>
        ))}
      </ProductListContent>
    </Container>
  );
}
