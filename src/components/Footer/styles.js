import styled from 'styled-components';

import { Colors } from '../../styles/variable';

export const Footer = styled.footer`
  display: flex;
  flex-direction: column;
  height: 150px;
  max-width: 90rem;
  margin: 0 auto;
  background-color: ${Colors.PRIMARY};
`;

export const ProductPaginationContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 0 10px;
`;
