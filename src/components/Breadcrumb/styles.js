import styled from 'styled-components';

import { Colors } from '../../styles/variable';

export const Container = styled.div`
  display: flex;
  align-items: center;
  padding: 20px 20px;
  max-width: 90rem;
  margin: 0 auto;

  & > :last-child {
    color: ${Colors.PRIMARY_DARK};
    margin-left: 5px;
  }
`;

export const BreadcrumbPages = styled.span`
  margin-right: 5px;
  color: ${Colors.PRIMARY_DARK};
`;
