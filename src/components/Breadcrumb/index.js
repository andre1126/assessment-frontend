import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { Container, BreadcrumbPages } from './styles';

export default function Breadcrumb({ page }) {
  return (
    <Container>
      <BreadcrumbPages>Página Inicial</BreadcrumbPages>
      {page && page !== 'Página Inicial' ? (
        <>
          <FontAwesomeIcon icon="chevron-right" size="xs" />
          <BreadcrumbPages>{page}</BreadcrumbPages>
        </>
      ) : null}
    </Container>
  );
}
