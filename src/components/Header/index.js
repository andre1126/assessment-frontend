import React, { useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Colors } from '../../styles/variable';

import {
  HigherBar,
  HigherBarText,
  Container,
  Logo,
  WebHeader,
  MobileHeader,
  MobileMenu,
  DrawerOption,
  DrawerButton,
  SearchContainer,
  SearchInput,
  SearchButton,
} from './styles';

import WebJumpLogo from '../../assets/images/logo.png';
import { ProductService } from '../../services/product';

export default function Header({ handleCategoryChange }) {
  const [isLargeScreen, setIsLargeScreen] = useState(false);
  const [isDrawerOpen, setIsDrawerOpen] = useState(false);
  const [data, setData] = useState([]);

  const handleDrawer = () => {
    setIsDrawerOpen(!isDrawerOpen);
  };

  const handleDrawerClick = (categoryId, pageName) => {
    handleCategoryChange(categoryId, pageName);
    handleDrawer();
  };

  const handleMediaQueryChange = (mediaQuery) => {
    if (mediaQuery.matches) {
      setIsLargeScreen(true);
    } else {
      setIsLargeScreen(false);
    }
  };

  useEffect(() => {
    const mediaQuery = window.matchMedia('(min-width: 768px)');
    mediaQuery.addListener(handleMediaQueryChange);
    handleMediaQueryChange(mediaQuery);

    return () => {
      mediaQuery.removeListener(handleMediaQueryChange);
    };
  }, []);

  useEffect(() => {
    const getData = async () => {
      const response = await ProductService.list();
      setData(response.items);
    };

    getData();
  }, []);

  return (
    <>
      {isLargeScreen ? (
        <>
          <HigherBar>
            <HigherBarText>Acesso sua Conta ou Cadastre-se</HigherBarText>
          </HigherBar>
          <Container>
            <WebHeader>
              <h1>
                <Logo src={WebJumpLogo} alt="WebJump" />
              </h1>
              <SearchContainer>
                <SearchInput />
                <SearchButton>BUSCAR</SearchButton>
              </SearchContainer>
            </WebHeader>
          </Container>
        </>
      ) : (
        <>
          <MobileMenu open={isDrawerOpen}>
            <DrawerButton onClick={handleDrawer}>
              <FontAwesomeIcon icon="times" size="2x" />
            </DrawerButton>
            <DrawerOption
              onClick={() => handleDrawerClick(1, 'Página Inicial')}
            >
              PÁGINA INICIAL
            </DrawerOption>
            {data.map((item, index) => (
              <DrawerOption
                key={index}
                onClick={() => handleDrawerClick(item.id, item.name)}
              >
                {item.name}
              </DrawerOption>
            ))}
            <DrawerOption onClick={handleDrawer}>CONTATOS</DrawerOption>
          </MobileMenu>
          <MobileHeader>
            <DrawerButton onClick={handleDrawer}>
              <FontAwesomeIcon icon="bars" size="2x" />
            </DrawerButton>
            <Logo src={WebJumpLogo} alt="WebJump" />
            <FontAwesomeIcon color={Colors.PRIMARY} icon="search" size="2x" />
          </MobileHeader>
        </>
      )}
    </>
  );
}
