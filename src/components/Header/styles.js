import styled from 'styled-components';

import { Colors } from '../../styles/variable';

export const HigherBar = styled.div`
  margin: 0 auto;
  background-color: ${Colors.BLACK};
`;

export const HigherBarText = styled.div`
  max-width: 90rem;
  margin: 0 auto;
  padding: 0 20px;
  text-align: right;
  color: ${Colors.WHITE};
`;

export const Container = styled.div`
  max-width: 90rem;
  margin: 0 auto;
  padding: 0 20px;
`;

export const Logo = styled.img`
  height: 40px;
  width: 160px;
`;

export const SearchContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 40px;
`;

export const SearchInput = styled.input`
  height: 100%;
  width: 350px;
`;

export const SearchButton = styled.button`
  background: ${Colors.PRIMARY};
  color: ${Colors.WHITE};
  padding: 10px 20px;
  text-align: center;
  font-weight: 800;
  font-size: 14px;
  transition: background 0.2s, color 0.2s;
  border: none;
  cursor: pointer;
`;

export const WebHeader = styled.header`
  height: 90px;
  display: flex;
  width: 100%;
  justify-content: space-between;
  align-items: center;

  button:focus {
    outline: none;
  }
`;

export const MobileHeader = styled.div`
  height: 12vh;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 5%;
`;

export const DrawerButton = styled.button`
  background: none;
  border: none;
  outline: none;

  &:active {
    transform: scale(1.2);
    transition: 0.1s;
  }
`;

export const DrawerOption = styled.button`
  background: none;
  border: none;
`;

export const MobileMenu = styled.div`
  display: flex;
  flex-direction: column;
  width: 70%;
  height: 100vh;
  border-right: 1px solid ${Colors.BLACK};
  background-color: ${Colors.WHITE};
  position: fixed;
  transform: ${({ open }) => (open ? 'translateX(0)' : 'TranslateX(-100%)')};
  transition: transform 0.3s ease-in-out;

  & > :first-child {
    padding: 6% 8%;
    text-align: left;
  }

  & > *:not(:first-child) {
    padding: 8% 8%;
    font-size: 24px;
  }
`;
