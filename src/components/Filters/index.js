import React from 'react';

import {
  Container,
  Title,
  FilterTitle,
  FiltersList,
  ColorButton,
  GenderButton,
  ClearButton,
} from './styles';

import { Colors } from '../../styles/variable';

export default function Filters({ categoryId, setSelectedFilter }) {
  const handleSelectFilter = (filterType, filterValue) => {
    setSelectedFilter({
      type: filterType,
      value: filterValue,
    });
  };

  const handleClearFilters = () => {
    setSelectedFilter({});
  };

  return (
    <Container>
      <Title>FILTRE POR</Title>
      {categoryId !== 2 ? (
        <>
          <FilterTitle>CORES</FilterTitle>
          <FiltersList>
            <ColorButton
              onClick={() => handleSelectFilter('color', 'Preta')}
              color={Colors.BLACK}
            />
            <ColorButton
              onClick={() => handleSelectFilter('color', 'Laranja')}
              color={Colors.ORANGE}
            />
            <ColorButton
              onClick={() => handleSelectFilter('color', 'Amarela')}
              color={Colors.YELLOW}
            />
            <ColorButton
              onClick={() => handleSelectFilter('color', 'Rosa')}
              color={Colors.PINK}
            />
            {categoryId === 3 ? (
              <>
                <ColorButton
                  onClick={() => handleSelectFilter('color', 'Azul')}
                  color={Colors.BLUE}
                />
                <ColorButton
                  onClick={() => handleSelectFilter('color', 'Bege')}
                  color={Colors.BEIGE}
                />
                <ColorButton
                  onClick={() => handleSelectFilter('color', 'Cinza')}
                  color={Colors.GRAY}
                />
              </>
            ) : null}
          </FiltersList>
        </>
      ) : null}
      {categoryId === 2 ? (
        <>
          <FilterTitle>GÊNERO</FilterTitle>
          <FiltersList>
            <GenderButton
              onClick={() => handleSelectFilter('gender', 'Masculina')}
              color={Colors.ORANGE}
            >
              Masculino
            </GenderButton>
            <GenderButton
              onClick={() => handleSelectFilter('gender', 'Feminina')}
              color={Colors.BLUE}
            >
              Feminino
            </GenderButton>
          </FiltersList>
        </>
      ) : null}
      <ClearButton onClick={handleClearFilters}>LIMPAR FILTROS</ClearButton>
    </Container>
  );
}
