import styled from 'styled-components';

import { Colors } from '../../styles/variable';

export const Container = styled.div`
  border: 1px solid ${Colors.GRAY};
  padding: 20px 30px;
  max-width: 250px;
  min-width: 220px;
  height: 440px;

  @media (max-width: 768px) {
    max-width: 100%;
    height: auto;
  }
`;

export const Title = styled.div`
  color: ${Colors.PRIMARY_DARK};
  font-size: 24px;
  font-weight: bold;
  margin: 10px 0;
`;

export const FilterTitle = styled.div`
  color: ${Colors.SECONDARY};
  font-size: 18px;
  margin: 25px 0 5px;
`;

export const GenderButton = styled.button`
  color: ${Colors.WHITE};
  background-color: ${({ color }) => (color ? color : Colors.PRIMARY)};
  margin: 10px 10px 0 0;
  padding: 10px;
  border: none;
  transition: background 0.2s, color 0.2s;
  cursor: pointer;
`;

export const ColorButton = styled.button`
  height: 20px;
  width: 50px;
  border: none;
  margin-right: 5px;
  background-color: ${({ color }) => (color ? color : Colors.WHITE)};

  @media (max-width: 768px) {
    height: 50px;
  }

  transition: background 0.2s, color 0.2s;
  cursor: pointer;
`;

export const ClearButton = styled.button`
  color: ${Colors.WHITE};
  background-color: ${Colors.PRIMARY};
  border: none;
  padding: 10px;
  margin-top: 20px;
  width: 100%;
  transition: background 0.2s, color 0.2s;
  cursor: pointer;

  @media (min-width: 320px) {
    &:hover {
      background: ${Colors.PRIMARY_DARK};
    }
  }
`;

export const FiltersList = styled.ul`
  list-style-position: inside;
`;
