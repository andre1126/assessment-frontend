# Desafio Frontend - WEBJUNP

## Tecnologias utilizadas

 - ReactJS
 - Styled Components
 - Axios
 - React Router

 Desenvolvido visando o conceito de SPA e utilizando a metodologia Mobile First.


## Instruções
Primeiro instale as dependências:

```
yarn
```
Para inicializar o frontend execute:

```
yarn start
```

Para inicializar o backend execute: 

```
yarn api
```
